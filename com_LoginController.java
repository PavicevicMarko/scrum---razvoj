package com;

import com.sun.source.doctree.TextTree;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class LoginController implements Initializable {

    @FXML
    private Label lblCredFalse;
    @FXML
    private TextField txtEmail;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private void btnLoginAction(javafx.event.ActionEvent event){
    if (txtEmail.getText().equals("test") && txtPassword.getText().equals("test")){
        lblCredFalse.setText("Welcome " + txtEmail.getText());
    }else{
        lblCredFalse.setText("Username or Password wrong!");
    }
    }

    @FXML
    private void btnRegisterAction(javafx.event.ActionEvent event) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("Register.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.setTitle("Register");
        stage.show();
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
